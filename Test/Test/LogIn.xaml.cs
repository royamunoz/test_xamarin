﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.ListView;
using Test.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LogIn : ContentPage
	{
		public LogIn ()
		{
			InitializeComponent ();
		}

	    async void LogIn_Clicked(object sender, System.EventArgs e)
	    {
	        await Navigation.PushAsync(new Despachar());
	    }

	    async void Register_Clicked(object sender, System.EventArgs e)
	    {
	        await DisplayAlert("Error", "Lo sentimos, aún no se pueden registrar nuevos usuarios","Aceptar");
	    }
    }
}