﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.ListView;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test.Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Tabbed : TabbedPage
	{
		public Tabbed()
		{
			InitializeComponent();
        }
	}
}