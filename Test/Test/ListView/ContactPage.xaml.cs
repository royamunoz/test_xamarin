﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test.ListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactPage : ContentPage
	{
	    async void Handle_ItemSelected(Object sender, SelectedItemChangedEventArgs e)
	    {
	        if (e.SelectedItem == null)
	        {
	            return;
	        }

	        var contact = e.SelectedItem as Contact;
	        await Navigation.PushAsync(new ContactDetailPage(contact));
	        listView.SelectedItem = null;
	    }
	

		public ContactPage ()
		{
			InitializeComponent ();

            listView.ItemsSource = new List<Contact>
            {
                new Contact{Name = "Roy", ImageUrl = "https://image.shutterstock.com/image-vector/male-user-account-profile-circle-450w-274504886.jpg"},
                new Contact{Name = "Daniel", ImageUrl = "https://image.shutterstock.com/image-vector/male-user-account-profile-circle-450w-274504886.jpg" }
            };
		}
	}
}