﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.ListView
{
    public class Contact
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string Status { get; set; }
    }
}
