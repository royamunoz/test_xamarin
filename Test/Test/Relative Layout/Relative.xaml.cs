﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test.Relative_Layout
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Relative : ContentPage
	{
		public Relative ()
		{
			InitializeComponent ();
		}
	}
}