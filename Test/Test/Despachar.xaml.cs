﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Despachar : ContentPage
	{
	    private IList<Grades> _grades;

	    public Despachar()
	    {
	        //_grades = GetGrades();

	        //foreach (var grade in _grades)
	        //{
	        //    Picker.Items.Add(grade.name);
	        //}

	        InitializeComponent();
	    }

        private IList<Grades> GetGrades()
	    {
	        return new List<Grades>
	        {
	            new Grades {name = "Super"},
	            new Grades {name = "Regular"},
	            new Grades {name = "Disel"}
	        };
	    }
		
	}
}