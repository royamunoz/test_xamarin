﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test
{
	public partial class GreetPage : ContentPage
	{
	    public int currentQuote;
	    string[] listaQuotes = { "a", "b", "c", "d" };

        public GreetPage()
	    {
	        currentQuote = 0;
	        InitializeComponent();
	        slider.Value = 1;
	        labelQuote.Text = listaQuotes[currentQuote];
	        int numBotones = 2;

	        switch (Device.RuntimePlatform)
	        {
                case Device.Android:
                    Padding = 30;
                    break;

                case Device.iOS:
                    Padding = 30;
                    break;

                default:
                    Padding = 10;
                    break;
	        }

	        for (int i = 0; i < numBotones; i++)
	        {
	            mainLayout.Children.Add(new Button{Text = "My Button"});
            }
            
	    }

	    private void Button_OnClicked(object sender, EventArgs e)
	    {
	        currentQuote++;
	        labelQuote.Text = listaQuotes[currentQuote % listaQuotes.Length];
	    }
    }
}