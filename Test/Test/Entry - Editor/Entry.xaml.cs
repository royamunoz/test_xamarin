﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test.Entry___Editor
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Entry : ContentPage
	{
		public Entry ()
		{
			InitializeComponent ();
		}

	    private void Entry_OnCompleted(object sender, EventArgs e)
	    {
	        Label.Text = "Is completed";
	    }

	    private void Selected(object sender, EventArgs e)
	    {
	        var selectedItem = Picker.Items[Picker.SelectedIndex];

            DisplayAlert("Selection", selectedItem,"Ok");
	    }
    }
}