﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Test.Popup
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Popups : ContentPage
	{
		public Popups ()
		{
			InitializeComponent ();
		}

	    public async void Button_OnClicked(object sender, EventArgs e)
	    {
	        var response1 = await DisplayActionSheet("Title", "Cancel", "Delete", "Copy", "Cut");

            switch (response1)
            {
                case "Cancel":
                    break;

                case "Delete":
                    var response2 = await DisplayAlert("Warning", "Are you sure?", "Yes", "No");
                    if (response2)
                    {
                        await DisplayAlert("Done", "Your response was deleted!", "Ok");
                    }
                    break;

                case "Copy":
                    await DisplayAlert("Done", "Your response was copied!", "Ok");
                    break;

                case "Cut":
                    await DisplayAlert("Done", "Your response was cut!", "Ok");
                    break;

                default:
                    break;
            }
        }
    }
}